import { ApiUri } from '../models/api-uri.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Artsuki } from '../models/artsuki';
import { Storage } from '@ionic/storage';

@Injectable()
export class AuthenticateService {
  constructor(private http: HttpClient, private storage: Storage) { }

  public authenticateMe(email: string, password: string): Promise<string> {
    const body = { email: email, password: password };
    console.log('Sending Authorization request ..');
    const response = this.http.post(
      ApiUri.URI + 'authentication',
      body,
      { headers: new HttpHeaders().set('Content-Type', 'application/json'), responseType: 'text' },
    );
    console.log('Geting TOKEN !');
    return response.toPromise();
  }

  getToken(email: string, password: string): string {
    this.authenticateMe(email, password);
    let tokenToReturn: string;
    this.storage.get('token').then(token => { tokenToReturn = token });
    return tokenToReturn;
  }

  getAuthenticatedUser(): Promise<Artsuki> {
    console.log('getting Conncted Artsuki from Api');
    return this.storage.get('token').then(token => {
        return this.http
          .get<Artsuki>(
            ApiUri.URI + 'artsuki',
             { headers: new HttpHeaders()
              .set('Authorization', 'Bearer ' + token)
              .set('Accept', '*/*')
             })
          .toPromise();
    });
  }

  authenticateMeRememberMeOptionCheck(email: string, password: string, rememberMe: boolean): Promise<string> {    
    this.storage.set('username', email);
    if ( rememberMe === true ) {
      this.storage.set('rememberMe', '1');
      this.storage.set('password', password);
    } else {
      this.storage.set('rememberMe', '0');
    }
    return this.authenticateMe(email, password);
  }
}
