import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Artwork } from '../../models/artwork';
import { ApiUri } from '../../models/api-uri.model';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';

@Injectable()
export class ArtworkProvider {
  storage: any;

  constructor(public http: HttpClient) {
    console.log('Hello ArtworkProvider Provider');
  }

  getArtwroks(): Observable<Artwork[]> {
    return this.http.get<Artwork[]>(ApiUri.URI + `artwork/main`)
      .pipe(
      catchError(this.handleError<Artwork[]>('getArtworks', []))
      );
  }

  getArtworkDetails(id: number): Observable<Artwork> {
    return this.http.get<Artwork>(ApiUri.URI + 'artwork/all?id=' + id);
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error); // log to console instead
      console.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }
}

