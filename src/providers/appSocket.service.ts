import { Storage } from '@ionic/storage';
import { Injectable } from '@angular/core';
import { Project } from '../models/project';
import { Artsuki } from '../models/artsuki';
import { Socket } from 'ng-socket-io';
import 'rxjs/add/operator/map'; 

@Injectable()
export class AppSocketService {
  connectedUser: Artsuki;
  loaded = false;
  constructor(private socket: Socket, private storage: Storage) {
    this.storage.get('connectedUser').then(res => {
        this.connectedUser = JSON.parse(res);
        this.loaded = true;
    })
    // this.socket = io(`http://localhost:3001`);
  }
  emitNotificationForSubscribe(project: Project) {
    console.log('emiting ..');
    const self = this;
    if (this.loaded) {
        this.socket.emit(
        'subscribe',
            {
              text: self.connectedUser.email + ' has just subscribed to your project',
              from: self.connectedUser.email,
              to: project.publisher.email
            });
    } else {
        this.emitNotificationForSubscribe(project);
    }
  }

  emitNotificationForAddCollaborator(project: Project, user: Artsuki) {
    console.log('emiting ..');
    const self = this;
    if (this.loaded) {
        this.socket.emit(
        'add-collaborator',
            {
              text: project.publisher.email + ' added you as a collaborator for his project: ' + project.title,
              from: project.publisher.email,
              to: user.email
            });
    } else {
        this.emitNotificationForAddCollaborator(project, user);
    }
  }

  getNotificationForSubscription() {
          return this.socket
            .fromEvent<any>('subscribe')
            .map(data => data);
  }

  getNotificationForCollaboration() {
    return this.socket
    .fromEvent<any>('add-collaborator')
    .map(data => data);
  }

}
