import { Event } from './../../models/event';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from "rxjs/Observable";
import { ApiUri } from "../../models/api-uri.model";
import { Storage } from '@ionic/storage';

/*
  Generated class for the EventProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class EventProvider {

  constructor(public http: HttpClient,private storage: Storage) {
    console.log('Hello EventProvider Provider');
  }

getEvents (): Observable<Event[]> {
    return this.http.get<Event[]>(ApiUri.URI + `event/all`);
  }
 
   getEventDetails(id: number): Observable<Event> {
    const body = {id: id};
    return this.http.post<Event>(ApiUri.URI + 'event/show', body);
  }
getEvent(parm: string): Observable<Event[]> {
   
    return this.http.get<Event[]>(ApiUri.URI + 'event/findBy?'+parm);
  }

  create(event:object): Promise<Observable<boolean>> {
    console.log('sending create request .. ');
    return this.getHeaders('json').then(res => {
    return this.http.post<string>(ApiUri.URI + 'event', event,res)
    .catch( (err) => this.handleErrors(err) );
    });  
  }
  private getHeadersWithContentType(responseType: string, contentType: string): Promise<any> {
    let header = this.storage.get('token').then(res => {
      return { headers: new HttpHeaders().set('Authorization', 'Bearer ' + res).set('Content-Type', contentType), responseType: responseType };
    });
    return header;
  }
   private getHeaders(responseType: string): Promise<any> {
    let header = this.storage.get('token').then(res => {
      return { headers: new HttpHeaders().set('Authorization', 'Bearer ' + res), responseType: responseType };
    });
    return header;
  }
  
  upload(event: FormData): Observable<boolean> {
    console.log('sending upload request .. ');
    console.log(ApiUri.dotNetUri + 'api/EventAPI/Post',event);
    return this.http.post<string>(ApiUri.dotNetUri + 'api/EventAPI/Post', event,{ headers: new HttpHeaders().set('Access-Control-Allow-Origin', '*')})
    .catch( (err) => this.handleErrors(err) );
  }
 
 delete(id: number):Promise<Observable<number>> {
   return this.getHeaders('json').then(res => {
    return this.http.delete<number>(ApiUri.URI + 'event/'+id,res).catch( (err) => this.handleErrors(err));
    }); 
  }
  find(id: number): Observable<any> {
    return this.http.get(ApiUri.URI + 'event/fin/'+id);
  }

    send(recipient: string, subject: string, message: string) {
    var body = new URLSearchParams()
    body.append("from", '***ahmed.esprit@esprit.tn***')
    body.append("to", recipient)
    body.append("subject", subject)
    body.append("html", message)
    return this.getHeadersWithContentType("Content-Type", "application/x-www-form-urlencoded") .then(res => {  
     return this.http.post(
      "https://api:***your api secret ***@api.mailgun.net/v3/" + "***your domain***" + "/messages",
      body,res)
    })
  }
Participe(id:number): Promise<Observable<any>>{

  let body = new URLSearchParams();
    body.set('eventID', `${ id }`);
    return this.getHeadersWithContentType('json', 'application/x-www-form-urlencoded')
    .then(res => {
      return this.http
      .put(ApiUri.URI + 'event/part', `eventID=${id}`, res);
    })
}
edit(event: Event): Observable<string> {
    return this.http.put<string>(ApiUri.URI + 'event', event);
  }

  private handleErrors(err) {
    return Observable.throw(err);
  }
  
}
