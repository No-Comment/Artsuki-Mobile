import { Storage } from '@ionic/storage';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import { of } from 'rxjs/observable/of';
import { ApiUri } from '../../models/api-uri.model';
import { catchError } from 'rxjs/operators';
import { Project } from '../../models/project';
import { Artsuki } from '../../models/artsuki';

/*
  Generated class for the ProjectProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ProjectProvider {

  constructor(public http: HttpClient, private storage: Storage) {
    console.log('Hello ProjectProvider Provider');
  }

  getProjects(page: number): Observable<Project[]> {
    return this.http.get<Project[]>(ApiUri.URI + `project?page=${page}`)
      .pipe(
      catchError(this.handleError<Project[]>('getProjects', []))
      );
  }

  getNbrPages(): Observable<{ number: number }> {
    return this.http.get<{ number: number }>(ApiUri.URI + `project?numberPage=1`)
      .pipe(
      catchError(this.handleError<any>('getNbrPages', 0))
      );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error); // log to console instead
      console.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }

  create(project: object): Promise<Observable<boolean>> {
    console.log('sending create request .. ');
    console.log(ApiUri.URI + 'project');
    return this.getHeaders('json').then(res => {
      return this.http.post<string>(ApiUri.URI + 'project', project, res)
        .catch((err) => this.handleErrors(err));
    });
  }

  private handleErrors(err) {
    return Observable.throw(err);
  }

  getProjectDetails(id: number): Observable<Project> {
    const body = { id: id };
    return this.http.post<Project>(ApiUri.URI + 'project/show', body);
  }

  edit(project: Project): Promise<Observable<string>> {
    return this.getHeaders('text as json').then(res => {
      return this.http.put<string>(ApiUri.URI + 'project', project, res)
      .catch((err) => this.handleErrors(err));
    });
  }

  delete(project: Project): Promise<any> {
    return this.getHeaders('text as json').then(res => {
      return this.http.delete<string>(ApiUri.URI + 'project/' + project.id, res);
    });
  }

  upload(formData: FormData) {
    this.http.post<boolean>(ApiUri.UploadPath, formData)
      .subscribe(ok => console.log('ok'));
  }

  private getHeaders(responseType: string): Promise<any> {
    let header = this.storage.get('token').then(res => {
      return { headers: new HttpHeaders().set('Authorization', 'Bearer ' + res), responseType: responseType };
    });
    return header;
  }

  private getHeadersWithContentType(responseType: string, contentType: string): Promise<any> {
    let header = this.storage.get('token').then(res => {
      return { headers: new HttpHeaders().set('Authorization', 'Bearer ' + res).set('Content-Type', contentType), responseType: responseType };
    });
    return header;
  }

  public getSubscribedProjects(user: Artsuki): Observable<Project[]> {
      return this.http.post<Project[]>(ApiUri.URI + 'project/subscribed', user);
  }

  public getMyProjects(user: Artsuki): Observable<Project[]> {
    return this.http.post<Project[]>(ApiUri.URI + 'project/perUser', user);
  }

  public unsubscribe(project: Project): Promise<Observable<any>> {
    let body = new URLSearchParams();
    body.set('project_id', `${ project.id }`);
    return this.getHeadersWithContentType('json', 'application/x-www-form-urlencoded')
    .then(res => {
      console.log(res);
      console.log(body.toString());
      return this.http
      .put(ApiUri.URI + 'project/subscribe/remove', `project_id=${project.id}`, res);
    })
  }

  public subscribe(project: Project): Promise<Observable<any>> {
    let body = new URLSearchParams();
    body.set('project_id', `${ project.id }`);
    return this.getHeadersWithContentType('json', 'application/x-www-form-urlencoded')
    .then(res => {
      return this.http
      .put(ApiUri.URI + 'project/subscribe/add', `project_id=${project.id}`, res);
    })
  }

  public getCollaborators(project: Project): Observable<Artsuki[]> {
    return this.http.post<Artsuki[]>(ApiUri.URI + 'project/collaborators', project);
  }

  public addCollaborator(projectId: number, artsukiId: number): Promise<Observable<any>> {
    const body = `projectId=${projectId}&artsukiId=${artsukiId}` ;
    console.log(body);
    return this.getHeadersWithContentType('json', 'application/x-www-form-urlencoded')
    .then(option => {
      return this.http.put<{message: string}>(ApiUri.URI + 'project/collaborators/add', body, option);
    });
  }

  public getListUsersForAutocomplete(): {email: string; id: number}[] {
    return [
      {email: 'artsuki5@artsuki.com', id: 27},
      {email: 'artsuki6@artsuki.com', id: 28},
      {email: 'artsuki7@artsuki.com', id: 29},
      {email: 'artsuki8@artsuki.com', id: 30},
      {email: 'artsuki9@artsuki.com', id: 31},
      {email: 'artsuki10@artsuki.com', id: 37},
      {email: 'artsuki11@artsuki.com', id: 38},
      {email: 'artsuki12@artsuki.com', id: 39},
      {email: 'artsuki13@artsuki.com', id: 40},
      {email: 'artsuki14@artsuki.com', id: 41},
      {email: 'artsuki15@artsuki.com', id: 47},
      {email: 'artsuki16@artsuki.com', id: 48},
      {email: 'artsuki17@artsuki.com', id: 49},
      {email: 'artsuki18@artsuki.com', id: 50},
      {email: 'artsuki19@artsuki.com', id: 51},
    ];
  }
}