import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Artsuki } from '../../models/artsuki';
import { ApiUri } from '../../models/api-uri.model';

/*
  Generated class for the ArtsukiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ArtsukiProvider {

  constructor(public http: HttpClient) {
  }
  register (artsuki: Artsuki) {
    return this.http.post<string>(ApiUri.URI+'artsuki/register',artsuki).subscribe();
  }
}
