import { DisplayEventPage } from './../event/display-event/display-event';
import { AuthenticateService } from './../../providers/authenticate.service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ListProjectPage } from '../project/list-project/list-project';
import { RegisterPage } from '../register/register';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  password: string;
  email: string;
  rememberMe: boolean;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private authService: AuthenticateService,
    private alertCtrl: AlertController,
    private storage: Storage
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  doLogin() {
    console.log(this.email);
    console.log(this.password);
    this.authService.authenticateMeRememberMeOptionCheck(this.email, this.password, this.rememberMe)
    .then(res => {
      console.log(res);
      this.storage.set('token', res).then(res => {
        this.authService.getAuthenticatedUser()
        .then(artsuki => {
          console.log(JSON.stringify(artsuki));
          this.storage.set('connectedUser', JSON.stringify(artsuki)).then(res => {
            if(this.navCtrl.canGoBack()) {
              this.navCtrl.pop();
            } else {
              this.navCtrl.setRoot(DisplayEventPage);
            }
          });
        });
      });
    })
    .catch(err => {
      this.showAlert('Please Check your credentials!');
    });
  }

  doSignup()
  {
    this.navCtrl.push(RegisterPage);
  }

  showAlert(message: string) {
    let alert = this.alertCtrl.create({
      title: 'Oops! Error Occured!',
      subTitle: message,
      buttons: ['OK'],
    });
    alert.present();
  }

}
