import { AuthenticateService } from './../../providers/authenticate.service';
import { ProjectPageModule } from './../project/project.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoginPage } from './login';
import { IonicStorageModule } from '@ionic/storage';

@NgModule({
  declarations: [
    LoginPage,
  ],
  imports: [
    IonicPageModule.forChild(LoginPage),
    ProjectPageModule,
    IonicStorageModule.forRoot(),
  ],
  providers: [AuthenticateService],
  exports: [LoginPage]
})
export class LoginPageModule {}
