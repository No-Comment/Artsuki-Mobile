import { Component } from '@angular/core';
import { NavController, NavParams, Loading } from 'ionic-angular';
import { ArtworkProvider } from '../../../providers/artwork/artwork';
import { Artwork } from '../../../models/artwork';
import { Artsuki } from '../../../models/artsuki';
import { ArtowrkDetailsPage } from '../artowrk-details/artowrk-details';
import { Storage } from '@ionic/storage';
import { ApiUri } from '../../../models/api-uri.model';

@Component({
  selector: 'page-artowrk-list',
  templateUrl: 'artowrk-list.html',
  providers: [ArtworkProvider]
})
export class ArtowrkListPage {
  artworks: Artwork[];
  connectedUser: Artsuki;
  loading: boolean;
  loader: Loading;
  uri = ApiUri.dotNetUri + 'Content/Upload/Portfolio/';

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private artworkService: ArtworkProvider,
    public storage: Storage
  ) { }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ArtowrkListPage');
    this.loading = true;
    this.storage.get('connectedUser').then(user => {
      this.connectedUser = JSON.parse(user);
      this.getArtworks();
    });
  }

  getArtworks() {
    this.artworkService.getArtwroks()
      .subscribe(res => {
        console.log('Artworks got');
        this.loading = false;
        this.artworks = res;
      },
      err => {
        console.log(err);
      });
  }

  doInfinite(infiniteScroll) {
    console.log('0');
    infiniteScroll.enable(false);
    this.artworkService.getArtwroks().subscribe(res => {
      res.forEach(art => this.artworks.push(art));
      infiniteScroll.enable(true);
    });
  }

  showArtworkDetails(artwork: Artwork) {
    this.navCtrl.push(ArtowrkDetailsPage, { artwork: artwork });
  }


}
