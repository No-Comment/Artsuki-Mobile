import { ArtowrkListPage } from './artowrk-list/artowrk-list';

export const ArtworkPages: Array<{ title: string, component: any}> = [
    { title: 'Artworks', component: ArtowrkListPage }
];