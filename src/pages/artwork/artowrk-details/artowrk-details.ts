import { Component } from '@angular/core';
import { NavController, NavParams, Loading, ToastController, LoadingController } from 'ionic-angular';
import { Artwork } from '../../../models/artwork';
import { Artsuki } from '../../../models/artsuki';
import { ApiUri } from '../../../models/api-uri.model';
import { ArtworkProvider } from '../../../providers/artwork/artwork';
import { Storage } from '@ionic/storage';
import { SocialSharing } from '@ionic-native/social-sharing';

/**
 * Generated class for the ArtowrkDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-artowrk-details',
  templateUrl: 'artowrk-details.html',
})
export class ArtowrkDetailsPage {
  artwork: Artwork;
  connectedUser: Artsuki;
  loading: Loading;
  loaded: boolean = false;
  uri = ApiUri.dotNetUri + 'Content/Upload/Portfolio/';

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private storage: Storage,
    public toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    public artworkService: ArtworkProvider,
    private socialSharing: SocialSharing
  ) {

    console.log('Init Artwork Details ');
    this.artwork = this.navParams.get('artwork');
    console.log(this.artwork);
    this.loading = this.loadingCtrl.create({
      content: 'Getting User',
    });
    this.loading.present();
    this.storage.get('connectedUser').then(connected => {
      this.connectedUser = JSON.parse(connected);
      console.log(this.connectedUser);
      this.loaded = true;
      this.loading.dismissAll();
    })
  }

  facebookShare(artwork) {
    var msg = artwork.title;
    var hint = artwork.description;
    var image = '0.0.0.0:3005/' + artwork.mediaPath;
    var url = "The url of the app"
    //this.socialSharing.shareViaFacebookWithPasteMessageHint(msg, image, image, hint);
    this.socialSharing.share(hint, msg, image, url);
  }


  regularShare(artwork: Artwork) {
    this.facebookShare(artwork);
  }

  ionViewDidLoad() {
    console.log(this.navParams.get('artwork'));
  }

}
