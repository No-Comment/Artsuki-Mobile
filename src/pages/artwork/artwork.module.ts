import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ArtworkPage } from './artwork';
import { ArtowrkDetailsPage } from './artowrk-details/artowrk-details';
import { ArtowrkListPage } from './artowrk-list/artowrk-list';
import { ComponentsModule } from '../../components/components.module';
import { SocialSharing } from '@ionic-native/social-sharing';

@NgModule({
  declarations: [
    ArtworkPage,
    ArtowrkListPage,
    ArtowrkDetailsPage
  ],
  imports: [
    IonicPageModule.forChild(ArtworkPage),
    ComponentsModule
  ], 
  entryComponents: [
    ArtowrkListPage,
    ArtowrkDetailsPage
  ],
  exports:[
    ArtowrkListPage,
    ArtowrkDetailsPage
  ]
})
export class ArtworkPageModule {}
