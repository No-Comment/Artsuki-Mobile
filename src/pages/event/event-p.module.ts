import { AddEventPage } from './add-event/add-event';
import { DetailEventPage } from './detail-event/detail-event';
import { DisplayEventPage } from './display-event/display-event';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EventPPage } from './event-p';
import { ComponentsModule } from "../../components/components.module";
import { EventProvider } from "../../providers/event/event";
import { EmailComposer } from '@ionic-native/email-composer';

@NgModule({
  declarations: [
    EventPPage,DisplayEventPage,DetailEventPage,AddEventPage,
  ],
  imports: [
    IonicPageModule.forChild(EventPPage),ComponentsModule,
  ],
  entryComponents: [DisplayEventPage,DetailEventPage,AddEventPage,
    
  ],
  providers: [
   EventProvider,EmailComposer
  ],
  exports: [
    DisplayEventPage,DetailEventPage,AddEventPage,
  ]
})
export class EventPPageModule {}
