import { Storage } from '@ionic/storage';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading, ToastController, LoadingController } from 'ionic-angular';
import { EventProvider } from "../../../providers/event/event";
import { Artsuki } from "../../../models/artsuki";
import { ApiUri } from "../../../models/api-uri.model";
import { DisplayEventPage } from "../display-event/display-event";
import { AlertController } from 'ionic-angular';
import { EmailComposer } from '@ionic-native/email-composer';

/**
 * Generated class for the DetailEventPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detail-event',
  templateUrl: 'detail-event.html',
})
export class DetailEventPage {
  evente: Event;
  connectedUser: Artsuki;
  loading: Loading;
  loaded: boolean = false;
  uri = ApiUri.dotNetUri + 'Content/Upload/Event/';
 constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private storage: Storage,
    public toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    public eventService: EventProvider,
    private alertCtrl: AlertController,
    private emailComposer: EmailComposer,
   
    ) {
    console.log('Init Event Details ');
     this.evente = this.navParams.get('event');
     console.log(this.evente);
     this.loading = this.loadingCtrl.create({
      content: 'Getting User',
    });
    this.loading.present();
     this.storage.get('connectedUser').then(connected => {
       this.connectedUser = JSON.parse(connected);
       console.log(this.connectedUser);
       this.loaded = true;
       this.loading.dismissAll();
     })
  


    }



delete(id:number){

  this.eventService.delete(id).then(res => {
      res.subscribe(res => {  
        console.log("successfully deleted!");
         this.navCtrl.last().dismiss();
         },
          err => { 
            console.log(err);
            this.navCtrl.push(DisplayEventPage);
          })
        });
}
      presentAlert(title:string) {
  let alert = this.alertCtrl.create({
    title: title,
    subTitle: ' ... ',
    buttons: ['Dismiss']
  });
  alert.present();
}
 presentConfirm(ids:number,title:string) {
  let alert = this.alertCtrl.create({
    title: 'Confirm Participation',
    message: 'Do you want to participate to Event :'+title,
    buttons: [
      {
        text: 'No',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Yes',
        handler: () => {
          this.participate(ids);
         
          console.log('Buy clicked');
        }
      }
    ]
  });
  alert.present();
}

  sendEmail() {
    let email = {
      to: 'ahmed.mabrouk@esprit.tn',
      cc: 'max@mustermann.de',
      subject: 'My Cool Image',
      body: 'Hey Simon, what do you thing about this image?',
      isHtml: true
    };
 
    this.emailComposer.open(email);
  }
participate(id:number):void{
  this.eventService.Participe(id).then(res => {
      res.subscribe(res => { 
      
         //this.navCtrl.last().dismiss();
         },
          err => { 
            console.log(err);
              if (err.status === 200) {
         
//this.eventService.send('ahmed.mabrouk@esprit.tn', 'Event', 'html code').then(res => {
     // res.subscribe()});
     this.sendEmail();
              this.presentAlert('succés'); 
           }else if(err.status===406) {       
          
             this.presentAlert('You already participated to this event'); 
           }else if(err.status===403) {       
     
             this.presentAlert('You can not participate to your own event'); 
           }else{
            this.presentAlert('You need to login first'); 

           }
          })
        });

}
      
  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailEventPage');
  }

}
