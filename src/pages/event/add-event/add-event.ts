import { Component, EventEmitter, Output } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading } from 'ionic-angular';
import { EventProvider } from "../../../providers/event/event";
import { Artsuki } from "../../../models/artsuki";
import { DisplayEventPage } from "../display-event/display-event";

/**
 * Generated class for the AddEventPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-event',
  templateUrl: 'add-event.html',
})
export class AddEventPage {
    loading: Loading;

  @Output() onAdd = new EventEmitter<Event>();
 title: string;
    categories = [
    { name: 'cinema' },
    { name: 'booksigning' }
  ];
  category: any;
  dateStart:string;
  dateEnd:string;
  datePub:string;
 types = [
    { name: 'limitedSeats' },
    { name: 'unlimitedSeats' }
  ];
  type: any;
  nbSeats: number;
  photo: File;
  places:string;
  description:string;
  connectedUser: Artsuki;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private eventService: EventProvider,
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddEventPage');
  }
  fileChange(event) {  

let fileList: FileList = event.target.files;  
if (fileList.length > 0) {  
let file: File = fileList[0];  
let formData: FormData = new FormData();  
formData.append('uploadFile', file, file.name);  
this.photo=file;
    this.eventService.upload(formData).subscribe(); 
 
}
  
} 
public addE() {
      //let photo: string;
    const event: object = {
      title: this.title,
      category: this.category,
      dateStart:this.dateStart,
      dateEnd:this.dateEnd,
      //datePub:this.datePub,
      type: this.type,
      nbSeats:this.nbSeats,
      //photo: this.photo.name,
      places:this.places,
      description: this.description,
    };
    this.eventService.create(event).then(res => {
      res.subscribe(res => {  
        console.log("successfully addded!");
         this.navCtrl.last().dismiss();
         },
          err => { 
            console.log(err);
            this.navCtrl.push(DisplayEventPage);
          })
        });
    
  }
}
