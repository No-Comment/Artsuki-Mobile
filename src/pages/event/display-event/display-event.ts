import { Event } from './../../../models/event';
import { AddEventPage } from './../add-event/add-event';
import { DetailEventPage } from './../detail-event/detail-event';
import { EventProvider } from './../../../providers/event/event';
import { Artsuki } from "../../../models/artsuki";
import { Component } from "@angular/core";
import { NavController, NavParams} from "ionic-angular";
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-display-event',
  templateUrl: 'display-event.html',
  providers: [EventProvider]
})
export class DisplayEventPage {
events:Event[];
  connectedUser: Artsuki;
   loading: boolean;
   constructor(
     public navCtrl: NavController,
     public navParams: NavParams,
     public storage: Storage ,
     private eventProvider: EventProvider
    ) { }

      ionViewDidLoad() {
    this.loading = true;
    this.storage.get('connectedUser').then(user => {
      this.connectedUser = JSON.parse(user);
      this.getEvent();
    });
  }

getEvent()
{
   this.eventProvider.getEvents().subscribe( ev => {
      this.loading = false;
      this.events = ev;
    });
}
   showDetails(event: Event) {
    this.navCtrl.push(DetailEventPage, { event: event });
  }
  addE() {
    this.navCtrl.push(AddEventPage);
  }
}