import { AppSocketService } from './../../../providers/appSocket.service';
import { Loading } from 'ionic-angular/components/loading/loading';
import { EditProjectPage } from './../edit-project/edit-project';
import { ProjectCollaboratorsPage } from './../project-collaborators/project-collaborators';
import { AddCollaboratorPage } from './../add-collaborator/add-collaborator';
import { ApiUri } from './../../../models/api-uri.model';
import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import { Project } from '../../../models/project';
import { Storage } from '@ionic/storage';
import { Artsuki } from '../../../models/artsuki';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { ProjectProvider } from './../../../providers/project/project';
import { Vibration } from '@ionic-native/vibration';
/**
 * Generated class for the ProjectDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-project-details',
  templateUrl: 'project-details.html',
})
export class ProjectDetailsPage {
  project: Project;
  connectedUser: Artsuki;
  loading: Loading;
  loaded: boolean = false;
  uri = ApiUri.dotNetUri + 'Content/Upload/Shop/';

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private storage: Storage,
    public toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    public projectService: ProjectProvider,
    private vibration: Vibration,
    private appSocket: AppSocketService
    ) {
    console.log('Init Project Details ');
     this.project = this.navParams.get('project');
     console.log(this.project);
     this.loading = this.loadingCtrl.create({
      content: 'Getting User',
    });
    this.loading.present();
     this.storage.get('connectedUser').then(connected => {
       this.connectedUser = JSON.parse(connected);
       console.log(this.connectedUser);
       this.loaded = true;
       this.loading.dismissAll();
     })
  }

  ionViewDidLoad() {
    console.log(this.navParams.get('project'));
  }

  editProject() {
    this.navCtrl.push(EditProjectPage, { project: this.project, connectedUser: this.connectedUser });
  }

  isHisOwner(): boolean {
    return this.connectedUser.id == this.project.publisher.id;
  }

  showCollaborators() {
    this.navCtrl.push(ProjectCollaboratorsPage, { project: this.project });
  }

  addCollaborator() {
    this.navCtrl.push(AddCollaboratorPage, { project: this.project });
  }

  toastPresent(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000
    });
    toast.present();
  }

  subscribe() {
    this.setLoading('sending subscribe request ..');
    this.projectService.subscribe(this.project)
    .then(observable => {
      observable.subscribe(res => {
        this.loading.dismissAll();
        this.toastPresent(res.message);
      }, err => { this.loading.dismissAll(); });
    });
  }

  delete() {
    this.projectService.delete(this.project).then(res => {
     res.subscribe(result => {
      this.toastPresent('your project has been deleted');
      this.navCtrl.pop();
     }, err => {
      this.toastPresent('error has been occured! please try again');
     });
    });
  }

  setLoading(message: string) {
    this.loading = this.loadingCtrl.create({
      content: message,
    });
    this.loading.present();
  }

  getSubscribeNotification() {
    this.appSocket.getNotificationForSubscription().subscribe(res => {
      if (res.to == this.connectedUser.email) {
        this.vibration.vibrate(3000);
        this.toastPresent(res.text.text);
      }
  });
  }

  getCollaborationNotification() {
    this.appSocket.getNotificationForCollaboration().subscribe(res => {
      if (res.to == this.connectedUser.email) {
        this.vibration.vibrate(3000);
        this.toastPresent(res.text.text);
      }
  });
  }
}
