import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { Project } from './../../../models/project';
import { Artsuki } from './../../../models/artsuki';
import { ProjectProvider } from './../../../providers/project/project';

/**
 * Generated class for the ProjectCollaboratorsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-project-collaborators',
  templateUrl: 'project-collaborators.html',
})
export class ProjectCollaboratorsPage {
  project: Project;
  users: Artsuki[];
  loaded = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private projectService: ProjectProvider,
    private alertCtrl: AlertController
  ) {
    this.project = this.navParams.get('project');
    this.projectService.getCollaborators(this.project)
    .subscribe(res => {
      if ( typeof res[0] !== 'undefined'  ) {
        this.users = res;
        this.loaded = true;
      } else {
        this.loaded = true;
        this.showAlert('This Project has no collaborator yet!');
      }
    },
    err => {
        console.log(err);
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProjectCollaboratorsPage');
  }

  isEmpty(): boolean {
    return typeof this.users === 'undefined';
  }

  showAlert(message: string) {
    let alert = this.alertCtrl.create({
      title: 'Message',
      subTitle: message,
      buttons: [
        {
          text: 'OK',
          handler: data => {
            this.navCtrl.last().dismiss();
          }
        }
      ]
    });
    alert.present();
  }
}
