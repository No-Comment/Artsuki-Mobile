import { AppSocketService } from './../../../providers/appSocket.service';
import { ApiUri } from './../../../models/api-uri.model';
import { ProjectProvider } from '../../../providers/project/project';
import { Component } from '@angular/core';
import { AlertController, NavController, NavParams, LoadingController, Loading } from 'ionic-angular';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { Project } from '../../../models/project';
import { Storage } from '@ionic/storage';
import { Artsuki } from '../../../models/artsuki';
import { Vibration } from '@ionic-native/vibration';
/**
 * Generated class for the MySubscribesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-my-subscribes',
  templateUrl: 'my-subscribes.html',
})
export class MySubscribesPage {
  projects: Project[];
  loaded = false;
  loading: Loading;
  connectedUser: Artsuki;
  uri = ApiUri.dotNetUri + 'Content/Upload/Shop/';
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private projectService: ProjectProvider,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController,
    private storage: Storage,
    private loadingCtrl: LoadingController,
    private appSocket: AppSocketService,
    private vibration: Vibration
  ) {
    this.storage.get('connectedUser')
    .then(res => {
      this.connectedUser = JSON.parse(res);
      this.projectService.getSubscribedProjects(this.connectedUser)
      .subscribe(
        res => {
          this.projects = res;
          this.loaded = true;
        },
        err => {
          this.showAlert('Connection with server failed! please check your connection');
        });
    })
    .catch(err => {

    });
  }

  showAlert(message: string) {
    let alert = this.alertCtrl.create({
      title: 'Error Occured!',
      subTitle: message,
      buttons: ['OK'],
    });
    alert.present();
  }

  presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000
    });
    toast.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MySubscribesPage');
  }

  getSubscribeNotification() {
    this.appSocket.getNotificationForSubscription().subscribe(res => {
      if (res.to == this.connectedUser.email) {
        this.presentToast(res.text.text);
      }
  });
  }

  getCollaborationNotification() {
    this.appSocket.getNotificationForCollaboration().subscribe(res => {
      if (res.to == this.connectedUser.email) {
        this.presentToast(res.text.text);
      }
  });
  }

  unsubscribe(project: Project) {
    this.setLoading('Unsubscribing ..');
    this.projectService.unsubscribe(project)
    .then(res => {
      res.subscribe(res => {
        this.loading.dismissAll();
        this.projects.splice(this.projects.indexOf(project, 0), 1);
        this.presentToast(res.message);
      },
      err => {
        console.log(err);
        this.loading.dismissAll();
        this.showAlert("Error in the server side ");
      })
    });
  }

  setLoading(message: string) {
    this.loading = this.loadingCtrl.create({
      content: message,
    });
    this.loading.present();
  }

}
