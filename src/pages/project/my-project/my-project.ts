import { AppSocketService } from './../../../providers/appSocket.service';
import { ApiUri } from './../../../models/api-uri.model';
import { LoginPage } from './../../login/login';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Project } from '../../../models/project';
import { Artsuki } from '../../../models/artsuki';
import { Storage } from '@ionic/storage';
import { ProjectProvider } from '../../../providers/project/project';
import { ActionSheetController } from 'ionic-angular/components/action-sheet/action-sheet-controller';
import { ProjectDetailsPage } from '../project-details/project-details';
import { EditProjectPage } from './../edit-project/edit-project';
import { Vibration } from '@ionic-native/vibration';
/**
 * Generated class for the MyProjectPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-my-project',
  templateUrl: 'my-project.html',
})
export class MyProjectPage {
  projects: Project[];
  connectedUser: Artsuki;
  loaded: boolean = false;
  uri = ApiUri.dotNetUri + 'Content/Upload/Shop/';
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private storage: Storage,
    private projectService: ProjectProvider,
    public toastCtrl: ToastController,
    public actionSheetCtrl: ActionSheetController,
    private appSocket: AppSocketService,
    private vibration: Vibration
  ) {
    this.storage.get('connectedUser')
    .then(res => {
      this.connectedUser = JSON.parse(res);
      this.projectService.getMyProjects(this.connectedUser)
      .subscribe(res => {
        this.projects = res;
        this.loaded = true;
      }, 
    err => {
      this.presentToast('Oops! Error has been occured! Please try again later');
    });
    })
    .catch(err => {
      console.log(err);
      this.navCtrl.push(LoginPage);
    });
  }

  presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000
    });
    toast.present();
  }

  getSubscribeNotification() {
    this.appSocket.getNotificationForSubscription().subscribe(res => {
      if (res.to == this.connectedUser.email) {
        this.vibration.vibrate(3000);
        this.presentToast(res.text.text);
      }
  });
  }

  getCollaborationNotification() {
    this.appSocket.getNotificationForCollaboration().subscribe(res => {
      if (res.to == this.connectedUser.email) {
        this.vibration.vibrate(3000);
        this.presentToast(res.text.text);
      }
  });
  }


  presentActionSheet(project: Project) {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'What you want to do ?',
      buttons: [
        {
          text: 'Show',
          icon: 'expand',
          handler: () => {
            this.showDetails(project);
          }
        },{
          text: 'Edit',
          icon: 'create',
          handler: () => {
            this.editProject(project);
        }
      },{
          text: 'Delete',
          icon: 'close',
          handler: () => {
            this.deleteProject(project);
          }
        }
      ]
    });
    actionSheet.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyProjectPage');
  }

  showDetails(project: Project) {
    this.navCtrl.push(ProjectDetailsPage, { project: project });
  }

  deleteProject(project: Project) {
    this.projectService.delete(project).then(res => {
      res.subscribe(result => {
        this.presentToast('your project has been removed');
      },
      err => {
        this.presentToast('Error with connection with the server! please try again!');
      });
    });
  }

  editProject(project: Project) {
    this.navCtrl.push(EditProjectPage, { project: project, connectedUser: this.connectedUser });
  }

}
