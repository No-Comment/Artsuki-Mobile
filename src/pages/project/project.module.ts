import { AppSocketService } from './../../providers/appSocket.service';
import { ProjectCollaboratorsPage } from './project-collaborators/project-collaborators';
import { MySubscribesPage } from './my-subscribes/my-subscribes';
import { MyProjectPage } from './my-project/my-project';
import { EditProjectPage } from './edit-project/edit-project';
import { AddProjectPage } from './add-project/add-project';
import { ProjectDetailsPage } from './project-details/project-details';
import { ComponentsModule } from '../../components/components.module';
import { ListProjectPage } from './list-project/list-project';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProjectPage } from './project';
import { ProjectProvider } from '../../providers/project/project';
import { AddCollaboratorPage } from './add-collaborator/add-collaborator';
import { NguiAutoCompleteModule } from '@ngui/auto-complete';
import { Vibration } from '@ionic-native/vibration';

@NgModule({
  declarations: [
    ProjectPage,
    ListProjectPage,
    ProjectDetailsPage,
    AddProjectPage,
    EditProjectPage,
    MyProjectPage,
    MySubscribesPage,
    ProjectCollaboratorsPage,
    AddCollaboratorPage
  ],
  imports: [
    IonicPageModule.forChild(ProjectPage),
    ComponentsModule,
    NguiAutoCompleteModule
  ],
  entryComponents: [ListProjectPage,
    ProjectDetailsPage,
    AddProjectPage,
    MyProjectPage,
    EditProjectPage,
    MySubscribesPage,
    ProjectCollaboratorsPage,
    AddCollaboratorPage
  ],
  providers: [
    ProjectProvider,
    AppSocketService,
    Vibration
  ],
  exports: [
    ListProjectPage,
    ProjectDetailsPage,
    EditProjectPage,
    MyProjectPage,
    MySubscribesPage,
    ProjectCollaboratorsPage,
    AddCollaboratorPage
  ]
})
export class ProjectPageModule {
  pages: Array<{title: string, component: any}>;
  
  constructor() {
    this.pages = [
      { title: 'Gallery', component: ListProjectPage }
    ]
  }
}
