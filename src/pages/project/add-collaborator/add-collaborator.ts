import { AppSocketService } from './../../../providers/appSocket.service';
import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController, ViewController } from 'ionic-angular';
import { Project } from './../../../models/Project';
import { ProjectProvider } from './../../../providers/project/project';
import { Loading } from 'ionic-angular/components/loading/loading';

/**
 * Generated class for the AddCollaboratorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-add-collaborator',
  templateUrl: 'add-collaborator.html',
})
export class AddCollaboratorPage {
  users: { email: string; id: number }[];
  usersDefault: { email: string; id:number }[];
  loading: Loading;
  showList = false;
  email: string;
  id: number;

  project: Project;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public projectService: ProjectProvider,
    public appSocket: AppSocketService,
    public toastCtrl: ToastController,
    public viewCtrl: ViewController,
    public loadingCtrl: LoadingController
    ) {
      console.log('hello there');
      this.users = this.projectService.getListUsersForAutocomplete();
      this.usersDefault = this.users;
      this.project = this.navParams.get('project');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddCollaboratorPage');
  }

  toastPresent(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000
    });
    toast.present();
  }

  addCollaborator() {
    this.setLoading('Adding new Collaborator ..');
    this.projectService.addCollaborator(this.project.id, this.getUserId())
    .then(observable => {
      observable.subscribe(res => {
        this.loading.dismissAll();
        this.toastPresent(res.message);
        this.navCtrl.popToRoot();
      }, 
      err => {
        this.toastPresent('Error While adding new Collaborator');
        this.loading.dismissAll();
        console.log(err);
        this.viewCtrl.dismiss();
      });
    });
  }

  filter() {
    console.log(this.email);
    this.users = this.usersDefault.filter(res =>  res.email.includes(this.email));
  }

  showAutocomplete() {
    this.showList = true;
  }

  usersEmail(): string[] {
    return this.users
    .map(res => {return res.email;});
  }

  setEmail(email: string) {
    console.log(email);
    this.email = email;
    this.showList = false;
  }

  getUserId(): number {
    const result = this.users.filter(res => res.email === this.email).map(res => { return res.id });
    return result[0];
  }

  hideAutoComplete() {
    this.showList = false;
  }

  setLoading(message: string) {
    this.loading = this.loadingCtrl.create({
      content: message,
    });
    this.loading.present();
  }
}
