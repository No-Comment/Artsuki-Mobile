import { MySubscribesPage } from './my-subscribes/my-subscribes';
import { MyProjectPage } from './my-project/my-project';
import { ListProjectPage } from './list-project/list-project';

export const ProjectPages: Array<{ title: string, component: any}> = [
    { title: 'Gallery', component: ListProjectPage },
    { title: 'My Projects', component: MyProjectPage },
    { title: 'My Subscribes', component: MySubscribesPage }
];