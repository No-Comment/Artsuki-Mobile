import { AppSocketService } from './../../../providers/appSocket.service';
import { ApiUri } from './../../../models/api-uri.model';
import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ActionSheetController } from 'ionic-angular';
import { File } from '@ionic-native/file';
import { Camera } from '@ionic-native/camera';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { Project } from '../../../models/project';
import { ProjectProvider } from '../../../providers/project/project';
import { Loading } from 'ionic-angular/components/loading/loading';
import { Platform } from 'ionic-angular/platform/platform';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { FilePath } from '@ionic-native/file-path';
import { LoginPage } from '../../login/login';
import { Artsuki } from '../../../models/artsuki';
import { Vibration } from '@ionic-native/vibration';
/**
 * Generated class for the AddProjectPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var cordova: any;
@Component({
  selector: 'page-add-project',
  templateUrl: 'add-project.html',
})
export class AddProjectPage {
  categories = [
    { name: 'ARCHITECTURE' },
    { name: 'SCULPTURE' },
    { name: 'PAINTING' },
    { name: 'MUSIC' },
    { name: 'LITERATURE' },
    { name: 'SCENE_ARTS' },
    { name: 'CINEMA' },
    { name: 'MEDIA_ARTS' },
    { name: 'ANIME_MANGA' },
    { name: 'DIGITAL_ART' },
    { name: 'PERSONAL' }
  ];
  project: Project = new Project();
  category: any;
  lastImage: string = null;
  loading: Loading;
  connectedUser: Artsuki;
  noUpload = true;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private camera: Camera,
    private transfer: FileTransfer,
    public actionSheetCtrl: ActionSheetController,
    private loadingCtrl: LoadingController,
    private file: File, 
    private projectService: ProjectProvider,
    private filePath: FilePath,
    public toastCtrl: ToastController,
    public platform: Platform,
    private appSocket: AppSocketService,
    private vibration: Vibration
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddProjectPage');
    this.connectedUser = this.navParams.get('connectedUser');
  }

  public presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Select Image Source',
      buttons: [
        {
          text: 'Load from Library',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Use Camera',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  public takePicture(sourceType) {
    this.platform.ready().then(res => {
      // Create options for the Camera Dialog
    var options = {
      quality: 100,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true,
      destinationType: this.camera.DestinationType.FILE_URI
    };
   
    // Get the data of an image
    this.camera.getPicture(options).then((imagePath) => {
      // Special handling for Android library
      if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
        this.filePath.resolveNativePath(imagePath)
          .then(filePath => {
            console.log(imagePath);
            let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
            let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
            this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
          });
      } else {
        console.log(imagePath);
        var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
        this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
      }
    }, (err) => {
      //this.presentToast('Error while selecting image.');
      console.log('Take picture: '+err);
    });
    });
  }

  private createFileName() {
    var d = new Date(),
    n = d.getTime(),
    newFileName =  n + ".jpg";
    this.project.photo = newFileName;
    return newFileName;
  }
   
  // Copy the image to a local folder
  private copyFileToLocalDir(namePath, currentName, newFileName) {
    this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
      this.lastImage = newFileName;
      this.noUpload = false;
    }, error => {
       console.log(error);
    });
  }

  public pathForImage(img) {
    if (img === null) {
      return '';
    } else {
      return cordova.file.dataDirectory + img;
    }
  }

  public addProject() {
    //this.uploadImage();
    this.setLoading('Creating project');
    this.projectService.create(this.project)
    .then(res => {
      res.subscribe(res => { 
        console.log("successfully addded!");
         this.loading.dismissAll();
         this.presentToast('Congratulation!! Project created successfully!');
         this.navCtrl.last().dismiss();
         },
          err => { 
            console.log('Error while creating Project'); 
            console.log(err);
            this.loading.dismissAll();
            this.presentToast('Oops! Error while Creating your project!');
            this.navCtrl.push(LoginPage);
          })
    });
  }

  presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000
    });
    toast.present();
  }

  public uploadImage() {
    // Destination URL
    var url = ApiUri.UploadPath;
   
    // File for Upload
    var targetPath = this.pathForImage(this.lastImage);
   
    // File name only
    var filename = this.lastImage;
   
    var options = {
      fileKey: "file",
      fileName: filename,
      chunkedMode: false,
      mimeType: "multipart/form-data",
      params : {'fileName': filename}
    };
   
    const fileTransfer: FileTransferObject = this.transfer.create();
   
    this.loading = this.loadingCtrl.create({
      content: 'Uploading...',
    });
    this.loading.present();
   
    // Use the FileTransfer to upload the image
    fileTransfer.upload(targetPath, url, options).then(data => {
      this.loading.dismissAll()
      console.log('Image succesful uploaded.');
      this.addProject();
    }, err => {
      this.loading.dismissAll()
      console.log(err);
    });
  }

  onSubmit() {
    console.log('onSubmit method invoked ..');
    this.uploadImage();
  }

  setLoading(message: string) {
    this.loading = this.loadingCtrl.create({
      content: message,
    });
    this.loading.present();
  }

  getSubscribeNotification() {
    this.appSocket.getNotificationForSubscription().subscribe(res => {
      if (res.to == this.connectedUser.email) {
        this.vibration.vibrate(3000);
        this.presentToast(res.text.text);
      }
  });
  }

  getCollaborationNotification() {
    this.appSocket.getNotificationForCollaboration().subscribe(res => {
      if (res.to == this.connectedUser.email) {
        this.vibration.vibrate(3000);
        this.presentToast(res.text.text);
      }
  });
  }
}
