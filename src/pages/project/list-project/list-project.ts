import { AppSocketService } from './../../../providers/appSocket.service';
import { AddProjectPage } from './../add-project/add-project';
import { ProjectDetailsPage } from './../project-details/project-details';
import { ProjectProvider } from './../../../providers/project/project';
import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, AlertController, LoadingController } from 'ionic-angular';
import { Project } from '../../../models/project';
import { Loading } from 'ionic-angular/components/loading/loading';
import { Artsuki } from '../../../models/artsuki';
import { Storage } from '@ionic/storage';
import { Vibration } from '@ionic-native/vibration';
/**
 * Generated class for the ListProjectPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-list-project',
  templateUrl: 'list-project.html',
  providers: [ProjectProvider, AppSocketService]
})
export class ListProjectPage {
  projects: Project[];
  connectedUser: Artsuki;
  page: number = 1;
  nbPages: number;
  loading: boolean;
  loader: Loading;

  constructor(
     public navCtrl: NavController,
     public navParams: NavParams,
     private projectService: ProjectProvider,
     private toastCtrl: ToastController,
     private alertCtrl: AlertController,
     private loadingCtrl: LoadingController,
     public storage: Storage,
     private appSocket: AppSocketService,
     private vibration: Vibration
    ) { }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListProjectPage');
    this.loading = true;
    this.storage.get('connectedUser').then(user => {
      this.connectedUser = JSON.parse(user);
      this.getProjects();
      this.getSubscribeNotification();
      this.getCollaborationNotification();
    });
  }

  getSubscribeNotification() {
    this.appSocket.getNotificationForSubscription().subscribe(res => {
      if (res.to == this.connectedUser.email) {
        this.vibration.vibrate(3000);
        this.toastPresent(res.text.text);
      }
  });
  }

  getCollaborationNotification() {
    this.appSocket.getNotificationForCollaboration().subscribe(res => {
      if (res.to == this.connectedUser.email) {
        this.vibration.vibrate(3000);
        this.toastPresent(res.text.text);
      }
  });
  }

  getProjects() {
    this.projectService.getProjects(this.page)
    .subscribe(res => {
      console.log('Projects gotten');
      this.loading = false;
      this.projectService.getNbrPages().subscribe(res => {
        this.nbPages = ( res.number / 10) > Math.floor( res.number / 10 )
        ? Math.floor( res.number / 10 ) + 1 
        : Math.floor(res.number / 10) ;
      })
      this.projects = res;
    },
    err => {
      console.log(err);
    });
  }

  getProjectsRefresher(refresher) {
    this.projectService.getProjects(this.page)
    .subscribe(res => {
      console.log('Projects gotten');
      this.loading = false;
      this.projectService.getNbrPages().subscribe(res => {
        this.nbPages = ( res.number / 10) > Math.floor( res.number / 10 )
        ? Math.floor( res.number / 10 ) + 1 
        : Math.floor(res.number / 10) ;
      })
      this.projects = res;
      refresher.complete();
    },
    err => {
      console.log(err);
    });
  }

  doInfinite(infiniteScroll) {
    console.log('0');
    if (this.page+1 > this.nbPages) {
      infiniteScroll.enable(false);
    } else{
      this.page += 1;
      infiniteScroll.enable(false);
       this.projectService.getProjects(this.page).subscribe(res => {
        res.forEach(proj => this.projects.push(proj));
        infiniteScroll.enable(true);
      });
    }
  }

  showDetails(project: Project) {
    this.navCtrl.push(ProjectDetailsPage, { project: project });
  }

  addProject() {
    this.navCtrl.push(AddProjectPage, { connectedUser: this.connectedUser });
  }

  toastPresent(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000
    });
    toast.present();
  }

  showAlert(message: string) {
    let alert = this.alertCtrl.create({
      title: 'Message',
      subTitle: message,
      buttons: [
        {
          text: 'OK',
          handler: data => {
            this.navCtrl.last().dismiss();
          }
        }
      ]
    });
    alert.present();
  }

  subscribe(project: Project) {
    console.log('Subscribe inside Lisre Project Liste');
    this.setLoading('Subscribing ..');
    this.projectService.subscribe(project)
    .then(observable => {
      observable.subscribe(res => {
        this.loader.dismissAll();
        this.toastPresent(res.message);
        this.appSocket.emitNotificationForSubscribe(project);
      },
      err => {
        this.loader.dismissAll();
        console.log(err);
      });
    });
  }

  setLoading(message: string) {
    this.loader = this.loadingCtrl.create({
      content: message,
    });
    this.loader.present();
  }

  doRefresh($event) {
    this.getProjectsRefresher($event);
  }

}
