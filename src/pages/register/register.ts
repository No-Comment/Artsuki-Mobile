import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Artsuki } from '../../models/artsuki';
import { ArtsukiProvider } from '../../providers/artsuki/artsuki';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  artsuki:Artsuki;

  constructor(public navCtrl: NavController, public navParams: NavParams,private artsukiService:ArtsukiProvider) {
    this.artsuki=new Artsuki;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  doSubmit()
  {
    this.artsukiService.register(this.artsuki);
this.navCtrl.pop();
  }

}
