import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SMS } from '@ionic-native/sms';
import { Artsuki } from '../../models/artsuki';

/**
 * Generated class for the InvitePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-invite',
  templateUrl: 'invite.html',
})
export class InvitePage {
  connectedUser: Artsuki;
  
  phonenumber: string;
  constructor(public navCtrl: NavController, public navParams: NavParams,private sms: SMS) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InvitePage');
  }

  send(){
    
    this.sms.send(this.phonenumber, 'Hi , this is an invitation to join artsukis community');
    
  }
}
