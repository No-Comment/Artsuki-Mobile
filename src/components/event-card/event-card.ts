import { Event } from './../../models/event';
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Artsuki } from "../../models/artsuki";
import { ApiUri } from "../../models/api-uri.model";

/**
 * Generated class for the EventCardComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'event-card',
  templateUrl: 'event-card.html'
})
export class EventCardComponent {
  @Input() evente: Event;

  uri = ApiUri.dotNetUri + 'Content/Upload/Event/';
  @Output() onDisplayEvent = new EventEmitter<Event>();
  @Output() onDelete = new EventEmitter<Event>();
  @Input() connectedUser: Artsuki;
  loading: boolean;
  text: string;

  constructor() {
    console.log('Hello EventCardComponent Component');
    this.text = 'Hello World';
  }

    displayEvent() {
    this.onDisplayEvent.emit(this.evente);
  
  }

}
