import { IonicModule } from 'ionic-angular';
import { NgModule } from '@angular/core';
import { ProjectCardComponent } from './project-card/project-card';
import { CommonModule } from '@angular/common';
import { SpinnerComponent } from './spinner/spinner';
import { ArtworkCardComponent } from './artwork-card/artwork-card';
import { EventCardComponent } from './event-card/event-card';
@NgModule({
	declarations: [ProjectCardComponent,
    SpinnerComponent,
    ArtworkCardComponent],
    EventCardComponent],
	imports: [IonicModule, CommonModule],
	exports: [ProjectCardComponent,SpinnerComponent,ArtworkCardComponent]
    EventCardComponent]
})
export class ComponentsModule {}
