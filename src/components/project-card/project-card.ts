import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Project } from '../../models/project';
import { ApiUri } from '../../models/api-uri.model';
import { Artsuki } from '../../models/artsuki';
/**
 * Generated class for the ProjectCardComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'project-card',
  templateUrl: 'project-card.html'
})
export class ProjectCardComponent {
  @Input() project: Project;
  text: string;
  @Output() whishlist = new EventEmitter<Project>();
  @Output() showDetails = new EventEmitter<Project>();
  uri = ApiUri.dotNetUri + 'Content/Upload/Shop/';
  @Input() connectedUser: Artsuki;
  loading: boolean;
  constructor() {}

  subscribe() {
    this.whishlist.emit(this.project);
  }

  showDetailsAction() {
    this.showDetails.emit(this.project);
  }
  
}
