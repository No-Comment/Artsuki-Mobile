import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Artwork } from '../../models/artwork';
import { ApiUri } from '../../models/api-uri.model';
import { Artsuki } from '../../models/artsuki';
/**
 * Generated class for the ProjectCardComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'artwork-card',
  templateUrl: 'artwork-card.html'
})
export class ArtworkCardComponent {
  @Input() artwork: Artwork;
  @Input() connectedUser: Artsuki;
  @Output() showArtworkDetails = new EventEmitter<Artwork>();
  text: string;
  uri = ApiUri.dotNetUri + 'Content/Upload/Portfolio/';
  loading: boolean;
  constructor() {}

  showDetailsAction() {
    this.showArtworkDetails.emit(this.artwork);
  }
}

