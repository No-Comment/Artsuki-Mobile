export class Artsuki {
    id: number;
    firstname: string;
    lastname: string;
    nationality: string;
    password: string;
    phoneNumber: string;
    username: string;
    gender: string;
    email: string;
    country: string;
    city: string;
    street: string;
    town: string;
    isVerified: boolean;
    role: string;
    biography: string;
    art: string;
}
