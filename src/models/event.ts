import { Artsuki } from './artsuki';
export class Event {
  id: number;
  publisher: Artsuki;
  title: string;
  category: string;
  dateStart:string;
  dateEnd:string;
  datePub:string;
  type: string;
  nbSeats: number;
  places:string;
  description:string;
  photo:string;
}
