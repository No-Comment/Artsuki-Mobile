export class ApiUri {
     public static URI = 'http://artsuki.ddns.net:18080/tn.esprit.twin.artsuki-web/api/';
     public static dotNetUri = 'http://artsuki.ddns.net/ArtsukiNet/';
     public static UploadPath = 'http://artsuki.ddns.net/ArtsukiNet/api/ProjectApi';
     public static SocketServer = 'http://artsuki.ddns.net:3001/';
}
