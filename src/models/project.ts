import { Artsuki } from './artsuki';
export class Project {
    id: number;
    description: string;
    photo: string;
    title: string;
    numberOfView: number;
    category: string;
    publisher: Artsuki;
}
