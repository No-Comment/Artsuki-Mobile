import { Artsuki } from './artsuki';
export class Artwork {
    id: number;
    title: string;
    datePub: Date;
    publicationType: string;
    publisher: Artsuki;
    category: string;
    description: string;
    mediaPath: string;
    forSale: boolean;
    price: number;
}
