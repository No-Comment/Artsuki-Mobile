import { AppSocketService } from './../providers/appSocket.service';
import { EventPPageModule } from './../pages/event/event-p.module';
import { ProjectPageModule } from '../pages/project/project.module';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { LoginPageModule } from '../pages/login/login.module';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { IonicStorageModule } from '@ionic/storage';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ProjectProvider } from '../providers/project/project';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { Camera } from '@ionic-native/camera';
import { FilePath } from '@ionic-native/file-path';
import { EventProvider } from '../providers/event/event';
import { SocketIoModule, SocketIoConfig } from 'ng-socket-io';
import { ApiUri } from '../models/api-uri.model';
import { ArtworkProvider } from '../providers/artwork/artwork';
import { ArtworkPageModule } from '../pages/artwork/artwork.module';
import { ArtowrkListPage } from '../pages/artwork/artowrk-list/artowrk-list';
import { SocialSharing } from '@ionic-native/social-sharing';
import { ArtowrkDetailsPage } from '../pages/artwork/artowrk-details/artowrk-details';
import { RegisterPage } from '../pages/register/register';
import { ArtsukiProvider } from '../providers/artsuki/artsuki';
import { InvitePage } from '../pages/invite/invite';
import { SMS } from '@ionic-native/sms';

const config: SocketIoConfig = { url: ApiUri.SocketServer, options: {} };

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    RegisterPage,
    InvitePage
  ],
  imports: [
    BrowserModule,
    FormsModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule,
    ProjectPageModule,
    ArtworkPageModule,
    LoginPageModule,
    HttpClientModule,
    SocketIoModule.forRoot(config) 
    EventPPageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    RegisterPage,
    InvitePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ProjectProvider,
    ArtworkProvider,
    FileTransfer,
    FilePath,
    FileTransferObject,
    File,
    Camera,
    AppSocketService
    SocialSharing
    ArtsukiProvider
    SMS,
    EventProvider
  ]
})
export class AppModule {}
