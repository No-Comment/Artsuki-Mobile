import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { Storage } from '@ionic/storage';
import { AppSocketService } from './../providers/appSocket.service';
import { LoginPage } from './../pages/login/login';
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Artsuki } from '../models/artsuki';
import { Events } from 'ionic-angular';
import { ArtworkPages } from '../pages/artwork/artwork-pages.menu';
import { InvitePage } from '../pages/invite/invite';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage;
  notifications: any;
  pages: Array<{title: string, component: any}>;
  connectedUser = new Artsuki();

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    private appSocket: AppSocketService,
    private toastCtrl: ToastController,
    private storage: Storage,
    private events: Events
    ) {
    this.initializeApp();
    // used for an example of ngFor and navigation
    this.pages = [];
    this.pages = this.pages.concat(ProjectPages);
    this.notifications = [];
    this.storage.get('connectedUser').then(res => {
      this.appSocket.getNotificationForSubscription().subscribe(res => {
            if (res.to == this.connectedUser.email) {
              this.presentToast(res.text.text);
            }
        });
    });
    this.pages = this.pages.concat(ArtworkPages);
    this.pages.push({title: 'invite',component : InvitePage});
    
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 300000
    });
    toast.present();
  }
}
